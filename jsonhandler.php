<?php
	
class ResumeBuilder {

    protected static $_messages = array(
        JSON_ERROR_NONE => 'No error has occurred',
        JSON_ERROR_DEPTH => 'The maximum stack depth has been exceeded',
        JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON',
        JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
        JSON_ERROR_SYNTAX => 'Syntax error, Malformed JSON',
        JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded'
    );


	/**
	* Return a string containing the JSON representation of value. 
	* @param	string  $utf8  UTF-8 character
	* @return string  UTF-8 character
	* @access public
	*/
	// public static function errorHandler()
	// {
		// throw new RuntimeException(static::$_messages[json_last_error()]);
		// echo static::$_messages[json_last_error()];
		// return true;
	// }

	
    public static function encode($value, $options = 0) {
		try{
        $result = json_encode($value, $options);

        if($result)  {
            return $result;
        }
		throw new RuntimeException();
		}
		catch(Exception $e) {
			echo "The Json File contains invalid Markup - (Error Detail :- <b>". static::$_messages[json_last_error()] . "</b>)";
		}
    }
	/**
	* Take a JSON encoded string and converts it into a PHP variable.  
	* @param	string  $utf8  UTF-8 character
	* @return array/object  UTF-8 character
	* @access public
	*/
	
    public static function decode($json, $assoc = false) {
		try{
        $result = json_decode(stripslashes($json), $assoc);

        if($result) {
            return $result;
        }
		throw new RuntimeException();
		}
		catch(Exception $e) {
			echo "The Json File contains invalid Markup - ( Error Detail :- <b>". static::$_messages[json_last_error()]."</b>)";
		}
    }
	public static function fetchIcon($socialNetwork) {
		switch($socialNetwork){
			case "Facebook":
				return "fa fa-facebook";
			break;
			case "Linkedin":
				return "fa fa-linkedin";
			break;
			case "Twitter":
				return "fa fa-twitter";
			break;
			default:
				return "fa fa-certificate";
		}
	}
	public static function processSocial($social){
		$i=0;
		if(is_array($social) && count($social)>0){
			foreach($social as $item){
				$socialData[]			=	$item;
				if($item['network']){
					$socialData[$i]['icon']	=	self::fetchIcon($item['network']);
				}
				$i++;
			}
		return $socialData;
		}
	}
}