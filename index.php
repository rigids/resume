<?php
/*
 * Application: Resume Generator from JSON
 * Description: Converts a data value node to Html based resume
 *
*/

/** The name of the database for WordPress */
define('JSONRESUME', 'jsonResume/resume.json');

/* Use jsonHandler class and view to segregate code */
require_once('View.php');
require_once('jsonhandler.php');


/* Sets up Application */
require_once('templates/header.php');
$jsonData 		=	file_get_contents(JSONRESUME);
$resume_data	=	ResumeBuilder::decode($jsonData,true);


/* This section controls which section to display */
$sections	  	=	array("basics","qualification","education","experience","portfolio","languages");

if(!empty($sections)) {
	foreach ($sections as $section) {
		if (!empty($section)) {
			View::render('templates/' . $section . '.php', (array)$resume_data[$section]);
		}
	}
}