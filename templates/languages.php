<section class="resume-section languages" itemscope itemtype="http://schema.org/ItemList" id="languages"><!-- languages -->
	<h2 class="resume-title" itemprop="name"><i class="fa fa-language"></i> Languages</h4>
	<div class="resume-item">
        <ul class="square">
<?php
 foreach ($viewData as $value) : ?>
		  <li class="htitle" itemprop="itemListElement"><span class="minwidth20"><?php echo $value['language']; ?></span>(<span class="expertise"><?php echo $value['fluency']; ?></span>)</li>
<?php endforeach; ?>
        </ul>
	</div>
</section><!-- /languages -->
