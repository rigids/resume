<section class="resume-section educations pt20 " id="educations"><!-- educations -->
	<h2 class="resume-title"><i class="fa fa-graduation-cap pb10"></i> Educations</h4>
	<div class="resume-item ">
	<?php if(is_array($viewData) && count($viewData)>0): ?>
		<table class="mytable education">
		<thead>
				<tr>
					<th class="tcol1">Degree</th>
					<th class="tcol2">Major</th>
					<th class="tcol3">Institution</th>
					<th class="tcol4" nowrap="">Graduation Year</th>
					<th class="tcol5">GPA/Percentage</th>
				</tr>
		</thead>
			<tbody>
		<?php foreach ($viewData as $value) : ?>
				<tr>
					<td class="icol1"><?php echo $value['studyType'] ?></td>
					<td class="icol2"><?php echo $value['area'] ?></td>
					<td class="icol3"><?php echo $value['institution'] ?></td>
					<td class="icol4" align="center"><?php echo $value['endYear'] ?></td>
					<td class="icol5"><?php echo $value['gpa'] ?></td>
				</tr>

		<?php endforeach; ?>
			</tbody>
		</table>
	<?php endif; ?>
	</div>
</section><!-- /educations -->
