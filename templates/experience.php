<section class="resume-section skills" itemscope itemtype="http://schema.org/ItemList" id="skills"><!-- skills -->
	<h2 class="resume-title" itemprop="name"><i class="fa fa-briefcase"></i> Professional Experience</h4>
	<div class="resume-item">
	<?php if(is_array($viewData) && count($viewData)>0): ?>
        <ul class="square">
			<?php foreach ($viewData as $value) : ?>
				<li class="htitle" itemprop="itemListElement"><span class="mr20 h4"><?php echo $value['company']; ?></span>
				<?php if(isset($value['startDate']) && !empty($value['startDate'])) : ?>
				(<span class=" "><?php echo $value['startDate']; ?></span> &ndash; <span class=" "><?php echo $value['endDate']; ?></span>)<br>
				<?php endif; ?>
				<div class="fontstyle"><?php echo $value['summary'] ?></div>
				<?php 
					$highlights	=	$value['highlights'];
					if(is_array($highlights) && count($highlights)>0) : ?>
					<ul class="circle pb20">
					<?php	foreach($highlights as $highlightitem): ?>
						<li><?php echo $highlightitem['responsibilities'] ?></li>
					<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
        </ul>
	<?php endif; ?>
	</div>
</section><!-- /skills -->
