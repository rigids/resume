<section class="resume-section projects" id="projects"><!-- projects -->
	<h2 class="resume-title"><i class="fa fa-cog"></i> Projects</h4>
	<div class="resume-item">
		<?php if(is_array($viewData) && count($viewData)>0): ?>
		<ul class="square">
			<?php foreach ($viewData as $value) : ?>
					<div class="vcard"><!-- project -->
						<li><a href="<?php echo $value['website']; ?>"><?php echo $value['website'] ?></a>		<?php 
								$technology	= @$value['technology']; 
									if(is_array($technology) && count($technology)>0):
								?>
									<?php	foreach($technology as $technologyitem): ?>
										<span class="listlabel"><?php echo $technologyitem; ?></span>
									<?php endforeach; ?>
								<?php endif; ?>
							<div class="pdescription">
								<?php 
									$description	= $value['description']; 
									if(is_array($description) && count($description)>0):
								?>
									<ul class="circle pb20">
										<?php	foreach($description as $descriptionitem): ?>
											<li><?php echo $descriptionitem; ?></li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>
							</div>
						
						</li>
					</div><!-- /project -->
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
	</div>
</section><!-- /projects -->
