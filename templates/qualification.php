<section class="resume-section qualification" id="publications"><!-- qualification -->
	<h2 class="resume-title"><i class="fa fa-user"></i> Qualifications Summary</h4>
	<div class="resume-item">
		<ul class="square">
			<?php 
			if(is_array($viewData['points']) && count($viewData['points'])>0):
			foreach($viewData['points'] as $value) : ?>
				<li><?php echo $value ?></li>
			<?php endforeach; ?>
			<?php endif; ?>
		</ul>
	</div>
</section><!-- qualification -->
