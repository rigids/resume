<header class="contact vcard clearfix" itemscope itemtype="http://schema.org/Person"><!-- header -->
	<?php
	if(isset($viewData['picture-url']) && !empty($viewData['picture-url'])) : ?>
	<figure class="alignleft image-container"><!-- figure -->
		<img src="<?php echo $viewData['picture-url']; ?>" alt="alt-caption" name="photo" itemprop="image" />
	</figure><!-- /figure -->
	<?php endif; ?>
	
	<hgroup role="banner" class="alignleft banner"> <!-- hgroup -->
		<?php if(isset($viewData['first-name']) && !empty($viewData['first-name'])) : ?>
			<h1 class="fn n" itemprop="name"><span class="given-name" itemprop="givenName"><?php echo $viewData['first-name']; ?></span> <span class="family-name" itemprop="familyName"><?php echo $viewData['last-name']; ?></span></h1>
		<?php endif; ?>
		
		<?php if(isset($viewData['label']) && !empty($viewData['label'])) : ?>
			<h2 class="role" itemprop="jobTitle"><?php echo $viewData['label']?></h2>
		<?php endif; ?>
		
		<?php 
		@$keywords	=	$viewData['key-skills'][0]['keywords'];
		if(is_array($keywords) && count($keywords)>0):
		?>
		<ul class='keywords'>
		<?php foreach($keywords as $skills){ ?>
				<li class="keyskills"><i class="fa fa-star-o"></i> <?php echo $skills?></li>
		<?php  } ?>
		</ul>
		<?php endif;  ?>	
		
	</hgroup> <!-- /hgroup -->
	
	
	<?php 
	$socialData		=	$viewData['profiles'];
	$social			=	ResumeBuilder::processSocial($socialData);
	?>
	
	<div class="alignright socialLinks">
		<ul >
		<?php if(isset($viewData['website']) && !empty($viewData['website'])) : ?>
			<li><a target="_blank" href="<?php echo $viewData['website']; ?>" title="Link to: <?php echo $viewData['website'] ?>" itemprop="url"><i class="fa fa-globe"></i><?php echo str_replace('http://', '', $viewData['website']); ?></a></li>
		<?php endif; ?>	
		<?php if(isset($viewData['email']) && !empty($viewData['email'])) : ?>
			<li><a target="_blank" class="email" href="mailto:<?php echo $viewData['email']; ?>" title="Email <?php echo $viewData['first-name']; ?> <?php echo $viewData['last-name']; ?>" itemprop="email"><i class="fa fa-envelope"></i><?php echo $viewData['email']; ?></a></li>
		<?php endif; ?>	
		
		<?php if(isset($viewData['phone']) && !empty($viewData['phone'])) : ?>
			<li><a target="_blank" class="phone" href="tel:<?php echo $viewData['phone']; ?>" title="Phone <?php echo $viewData['first-name']; ?> <?php echo $viewData['last-name']; ?>" itemprop="telephone"><i class="fa fa-phone"></i><?php echo $viewData['phone']; ?></a></li>
		<?php endif; ?>	
		
		

		<?php
		$i=0;
		if(is_array($social) && count($social)>0){
			foreach($social as $item){
				if($item['url']){
				?>
					<li><a target="_blank" class="<?php echo $item['network'] ?>"  href="<?php echo $item['url'] ?>"><i class="<?php echo $item['icon'] ?>"></i><?php echo $item['text'] ?></a></li>
				<?php
				}
			}
		}
		?>
	</div>

</header><!-- /header -->


